Patches for libgcrypt11-1.5.x:

Source: http://security.debian.org/debian-security/pool/updates/main/libg/libgcrypt11/libgcrypt11_1.5.0-5+deb7u6.debian.tar.gz

Renamed 22_* to 46_* (to properly order the patches - no idea why they
are numbered like this..)

Deleted several others (29,30,31,32,35,36,41,42) because they are already
in 1.5.3 (our base version).
