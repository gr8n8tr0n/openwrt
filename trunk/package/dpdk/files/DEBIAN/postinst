#!/bin/sh

#DEBHELPER#

NAME=dpdk
VERSION=__BASE_VERSION__
MAXPAGES=0
ARCH=`dpkg --print-architecture`

# Do not update this API directly, this is a mirror of the API
# with the same name in gateway/install/opt/vc/bin/vc_dpdk.sh
# So changes there should reflect here
adjust_memory_parameters()
{
    freemem=`free | awk 'FNR == 2 {print $2}'`
    if [ $freemem -lt 3800000 ];
    then
        # < 4Gig mem, dpdk is 1 Gig
        MAXPAGES=512
    elif [ $freemem -lt 32000000 ];
    then
        # >4Gig < 32Gig mem, dpdk is 2Gig
        MAXPAGES=1024
    else
        # >= 32Gig mem, dpdk is 4Gig
        MAXPAGES=2048
    fi
}

dkms_configure () {
    POSTINST=/usr/lib/dkms/common.postinst
    if [ -f "$POSTINST" ]; then
        "$POSTINST" "$NAME" "$VERSION" "/dev/null/nonexistent" "$ARCH" "$2"
        return $?
    fi
    echo "WARNING: $POSTINST does not exist." >&2
    echo "ERROR: DKMS version is too old" >&2
    return 1
}

case "$1" in
        configure)
		set -o errexit
                # calculate max hugepages for current memory size
                adjust_memory_parameters

                echo "Adding module to DKMS"
                dkms_configure

                echo "Adding hugepages to grub kernel config"
                grub=`cat /etc/default/grub | sed -rn 's/^[\s]*GRUB_CMDLINE_LINUX[\s]*=[\s]*\"(.*)\"/\1/p'`
                if [ "$grub" = "" ];
                then
                    sed -i "s/^[\s]*GRUB_CMDLINE_LINUX=.*/GRUB_CMDLINE_LINUX=\"default_hugepagesz=2M hugepagesz=2M hugepages=$MAXPAGES\"/" /etc/default/grub
                else
                    grub=`echo $grub | sed -re "s/default_hugepagesz[\s]*=[\s]*[0-9]+[A-Z]//g"`
                    grub=`echo $grub | sed -re "s/hugepagesz[\s]*=[\s]*[0-9]+[A-Z]//g"`
                    grub=`echo $grub | sed -re "s/hugepages[\s]*=[\s]*[0-9]+//g"`
                    sed -i "s/^[\s]*GRUB_CMDLINE_LINUX=.*/GRUB_CMDLINE_LINUX=\"$grub default_hugepagesz=2M hugepagesz=2M hugepages=$MAXPAGES\"/" /etc/default/grub
                fi
                update-grub

                echo "Adding DPDK modules to startup config"
                egrep -q '^uio$' /etc/modules || echo 'uio' >> /etc/modules
                egrep -q '^igb_uio$' /etc/modules || echo 'igb_uio' >> /etc/modules
                egrep -q '^rte_kni$' /etc/modules || echo 'rte_kni' >> /etc/modules

                echo "Done."
        ;;
esac
