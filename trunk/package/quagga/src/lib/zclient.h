/* Zebra's client header.
 * Copyright (C) 1999 Kunihiro Ishiguro
 *
 * This file is part of GNU Zebra.
 *
 * GNU Zebra is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 * 
 * GNU Zebra is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with GNU Zebra; see the file COPYING.  If not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef _ZEBRA_ZCLIENT_H
#define _ZEBRA_ZCLIENT_H

/* For struct zapi_ipv{4,6}. */
#include "prefix.h"

/* For struct interface and struct connected. */
#include "if.h"

/* For input/output buffer to zebra. */
#define ZEBRA_MAX_PACKET_SIZ          4096

/* Zebra header size. */
#define ZEBRA_HEADER_SIZE             6

/* Structure for the zebra client. */
struct zclient
{
  /* Socket to zebra daemon. */
  int sock;

  /* Flag of communication to zebra is enabled or not.  Default is on.
     This flag is disabled by `no router zebra' statement. */
  int enable;

  /* Connection failure count. */
  int fail;

  /* Input buffer for zebra message. */
  struct stream *ibuf;

  /* Output buffer for zebra message. */
  struct stream *obuf;

  /* Buffer of data waiting to be written to zebra. */
  struct buffer *wb;

  /* Read and connect thread. */
  struct thread *t_read;
  struct thread *t_connect;

  /* Thread to write buffered data to zebra. */
  struct thread *t_write;

  /* Redistribute information. */
  u_char redist_default;
  u_char redist[ZEBRA_ROUTE_MAX];

  /* Redistribute defauilt. */
  u_char default_information;

  /* Pointer to the callback functions. */
  void (*zebra_connected) (struct zclient *);
  int (*router_id_update) (int, struct zclient *, uint16_t);
  int (*interface_add) (int, struct zclient *, uint16_t);
  int (*interface_delete) (int, struct zclient *, uint16_t);
  int (*interface_up) (int, struct zclient *, uint16_t);
  int (*interface_down) (int, struct zclient *, uint16_t);
  int (*interface_address_add) (int, struct zclient *, uint16_t);
  int (*interface_address_delete) (int, struct zclient *, uint16_t);
  int (*ipv4_route_add) (int, struct zclient *, uint16_t);
  int (*ipv4_route_delete) (int, struct zclient *, uint16_t);
  int (*ipv6_route_add) (int, struct zclient *, uint16_t);
  int (*ipv6_route_delete) (int, struct zclient *, uint16_t);
  int (*nexthop_update) (int, struct zclient *, uint16_t);
#ifdef HAVE_ZEBRA_MQ
  int (*proto_mq_recv) (int, struct zclient *, uint16_t);
#endif
};

/* Zebra API message flag. */
#define ZAPI_MESSAGE_NEXTHOP  0x01
#define ZAPI_MESSAGE_IFINDEX  0x02
#define ZAPI_MESSAGE_DISTANCE 0x04
#define ZAPI_MESSAGE_METRIC   0x08
#define ZAPI_MESSAGE_TAG      0x10
//#ifdef HAVE_ZEBRA_MQ
// Protocol specific info
#define ZAPI_MESSAGE_PROTO1   0x40 // ASPATH for BGP, cost for OSPF
#define ZAPI_MESSAGE_PROTO2   0x80 //COMMUNITY for BGP, metric-type for OSPF
#define ZAPI_MESSAGE_ASPATH   ZAPI_MESSAGE_PROTO1
#define ZAPI_MESSAGE_COMMUNITIES ZAPI_MESSAGE_PROTO2
//#endif

/* Zebra API message redis flag. */
#define ZAPI_REDIS_PREF_NON_USER  0x01

/* Maximum as-paths transported over ZAPI */
#define ZAPI_ASPATH_LEN_MAX  24

/* Zserv protocol message header */
struct zserv_header
{
  uint16_t length;
  uint8_t marker;	/* corresponds to command field in old zserv
                         * always set to 255 in new zserv.
                         */
  uint8_t version;
#define ZSERV_VERSION	2
  uint16_t command;
};

struct zapi_bgp_attr
{
  u_int32_t local_pref;
  u_char    aspath_len;
  u_int32_t aspath_val[ZAPI_ASPATH_LEN_MAX + 1];
  u_int32_t community_size;
  u_int32_t *community_val;
};

/* Zebra IPv4 route message API. */
struct zapi_ipv4
{
  char iname[INSTANCE_NAMSIZ];

  u_char type;

  u_char flags;

  u_char message;

  safi_t safi;

  u_char nexthop_num;
  struct in_addr **nexthop;

  u_char ifindex_num;
  unsigned int *ifindex;

  u_char distance;

  u_int32_t metric;

  u_short tag;

  void *proto_data;
};

struct zapi_redis
{
  char iname[INSTANCE_NAMSIZ];
  int type;
};

#ifndef HAVE_VIFI_T
typedef unsigned short vifi_t;
#endif

#ifdef HAVE_ZEBRA_MQ
#define ZEB_MAXVIFS (256 + (256 * 4)) // 256 underlay + 1K overlay support

struct zeb_mfcctl {
  struct in_addr mfcc_origin;             /* Origin of mcast      */
  struct in_addr mfcc_mcastgrp;           /* Group in question    */
  vifi_t         mfcc_parent;             /* Where it arrived     */
  unsigned char  mfcc_ttls[ZEB_MAXVIFS];  /* Where it is going    */
  unsigned int   mfcc_pkt_cnt;            /* pkt count for src-grp */
  unsigned int   mfcc_byte_cnt;
  unsigned int   mfcc_wrong_if;
  int            mfcc_expire;
};

struct zeb_pim_nbr {
  struct in_addr ipaddr;             /* IP address of neighbor */
  vifi_t         intf;               /* Where it is learnt */
  int            dr_priority;
};
#endif


/* Prototypes of zebra client service functions. */
extern struct zclient *zclient_new (void);
extern void zclient_init (struct zclient *, int);
extern int zclient_start (struct zclient *);
extern void zclient_stop (struct zclient *);
extern void zclient_reset (struct zclient *);
extern void zclient_free (struct zclient *);
extern void zclient_interface_request (struct zclient *);

extern int  zclient_socket_connect (struct zclient *);
extern void zclient_serv_path_set  (char *path);
extern const char *const zclient_serv_path_get (void);

/* Send redistribute command to zebra daemon. Do not update zclient state. */
extern int zebra_redistribute_send (int command, struct zclient *, int type, struct zapi_redis *api);

/* If state has changed, update state and call zebra_redistribute_send. */
extern void zclient_redistribute (int command, struct zclient *, int type);

/* If state has changed, update state and send the command to zebra. */
extern void zclient_redistribute_default (int command, struct zclient *);

/* Send the message in zclient->obuf to the zebra daemon (or enqueue it).
   Returns 0 for success or -1 on an I/O error. */
extern int zclient_send_message(struct zclient *);

/* create header for command, length to be filled in by user later */
extern void zclient_create_header (struct stream *, uint16_t);

extern struct interface *zebra_interface_add_read (struct stream *);
extern struct interface *zebra_interface_state_read (struct stream *s);
extern struct connected *zebra_interface_address_read (int, struct stream *);
extern void zebra_interface_if_set_value (struct stream *, struct interface *);
extern void zebra_router_id_update_read (struct stream *s, char *iname, struct prefix *rid);
extern int zapi_ipv4_route (u_char, struct zclient *, struct prefix_ipv4 *, 
                            struct zapi_ipv4 *);
extern struct interface *
zebra_interface_add_read_vrf (struct stream *s, vrf_id_t vrf_id);
extern struct interface *
zebra_interface_state_read_vrf (struct stream *s, vrf_id_t vrf_id);
extern struct connected *
zebra_interface_address_read_vrf (int type, struct stream *s, vrf_id_t vrf_id);
extern void
zclient_init_vrf (struct zclient *zclient, int redist_default, u_short id);
extern void
zclient_redistribute_default_vrf (int command, struct zclient *zclient, vrf_id_t vrf_id);
extern int
zclient_read_header (struct stream *s, int sock, u_int16_t *size, u_char *marker,
                     u_char *version, vrf_id_t *vrf_id, u_int16_t *cmd);
extern void
zclient_create_header_vrf (struct stream *s, uint16_t command, vrf_id_t vrf_id);
#ifdef HAVE_ZEBRA_MQ
struct mfcctl;
#else
struct zeb_mfcctl;
#endif
struct vifctl;
extern int
zclient_send_mrt_init(struct zclient *zclient);
extern int
zclient_send_mrt_done(struct zclient *zclient);
extern int
zclient_send_igmpmsg_wrvifwhole(struct zclient *zclient);
extern int
zclient_send_ip_multicast_if(struct zclient *zclient, struct ip_mreqn *mreq);
extern int
zclient_send_ip_add_membership(struct zclient *zclient, uint32_t grp_addr, uint32_t ifindex);
extern int
zclient_send_ip_del_membership(struct zclient *zclient, uint32_t grp_addr, uint32_t ifindex);
#ifdef HAVE_ZEBRA_MQ
extern int
zclient_send_mrt_add_mfc(struct zclient *zclient, struct zeb_mfcctl *oil);
extern int
zclient_send_mrt_del_mfc(struct zclient *zclient, struct zeb_mfcctl *oil);
extern int
zclient_send_mrt_add_pim_nbr(struct zclient *zclient, struct zeb_pim_nbr *pnbr);
extern int
zclient_send_mrt_del_pim_nbr(struct zclient *zclient, struct zeb_pim_nbr *pnbr);
#else
extern int
zclient_send_mrt_add_mfc(struct zclient *zclient, struct mfcctl *oil);
extern int
zclient_send_mrt_del_mfc(struct zclient *zclient, struct mfcctl *oil);
#endif
extern int
zclient_send_mcast_join_source_group(struct zclient *zclient, uint32_t group_addr, uint32_t source_addr, uint32_t
        ifindex);
extern int
zclient_send_mrt_add_vif(struct zclient *zclient, struct vifctl *vc);
extern int
zclient_send_mrt_del_vif(struct zclient *zclient, struct vifctl *vc);

#ifdef HAVE_IPV6
/* IPv6 prefix add and delete function prototype. */

struct zapi_ipv6
{
  char iname[INSTANCE_NAMSIZ];

  u_char type;

  u_char flags;

  u_char message;

  safi_t safi;

  u_char nexthop_num;
  struct in6_addr **nexthop;

  u_char ifindex_num;
  unsigned int *ifindex;

  u_char distance;

  u_int32_t metric;
  
  u_short tag;

  void *proto_data;
};

extern int zapi_ipv6_route (u_char cmd, struct zclient *zclient, 
                     struct prefix_ipv6 *p, struct zapi_ipv6 *api);
#endif /* HAVE_IPV6 */

#endif /* _ZEBRA_ZCLIENT_H */
