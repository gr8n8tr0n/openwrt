# Common packages:
DEFAULT_PACKAGES += \
	bridge \
	ip \
	iputils-ping \
	iputils-ping6 \
	traceroute \
	iputils-traceroute6 \
	iputils-tracepath \
	iputils-tracepath6 \
	kmod-igb \
	kmod-intel-i40e \
	kmod-intel-ixgbe \
	net-tools-arp \
	net-tools-ifconfig \
	net-tools-netstat \
	net-tools-route \
	openssh-server-pam \
	openssh-client \
	openssl-util \
	uuidgen \

# Common modules:
DEFAULT_PACKAGES += \
	kmod-fs-ext4 \
	kmod-loop \
