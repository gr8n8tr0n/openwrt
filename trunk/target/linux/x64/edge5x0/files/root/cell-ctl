#!/bin/sh
#set -x

slotb=/sys/devices/platform/vc/slotb
usbdev=/sys/bus/usb/devices/1-1.3
qmidev=/dev/cdc-wdm0
atdev=/dev/ttyUSB2

# wait for modem to be gone;
# $1 is timeout in secs;

wait_off() {
	to=$1;
	while [ $to -gt 0 -a -L $usbdev ]; do sleep 1; to=$(($to - 1)); done;
	if [ $to -lt 0 ]; then
		echo 'error: modem did not turn off'
		exit 1
	fi
}

# wait for modem to be there;
# $1 is timeout in secs;

wait_on() {
	to=$1;
	while [ $to -gt 0 -a ! -L $usbdev ]; do sleep 1; to=$(($to - 1)); done;
	if [ $to -lt 0 ]; then
		echo 'error: modem did not turn off'
		exit 2
	fi
}

# idle all modem handling;

cell_kill() {
	/etc/init.d/ModemManager stop > /dev/null 2>&1
	killall -q mbim-proxy qmi-proxy mm_run.sh
}

# turn modem off;

cell_off() {
	echo 0 > $slotb/reset
	echo 0 > $slotb/power
	echo 0 > $slotb/sim_enable
	wait_off 15
}

# turn modem on;
# $1 is sim_enable;

cell_on() {
	echo $1 > $slotb/sim_enable
	echo 1 > $slotb/reset
	echo 1 > $slotb/power
	wait_on 30
}

# soft-reset modem;

cell_sreset() {
	qmicli -d $qmidev --dms-set-operating-mode=offline
	qmicli -d $qmidev --dms-set-operating-mode=reset
	wait_off 15
	wait_on 30
	sleep 5
}

# set usb composition;
# $1 is composition number;

set_usb() {
	qmicli -d $qmidev --dms-swi-set-usb-composition=$1
	cell_sreset
	qmicli -d $qmidev --dms-swi-get-usb-composition
}

# save original hotplug;

if [ ! -r /root/30-velocloud-modem-tty ]; then
	cp /etc/hotplug.d/tty/30-velocloud-modem-tty /root/30-velocloud-modem-tty
fi

# process options;

while [ $# -gt 2 ]; do
	case $1 in
	-q)
		if [ -z "$2" ]; then
			echo 'error: [-q] missing QMI device name'
			exit 4
		fi
		qmidev=$2
		shift
		shift
		;;
	-a)
		if [ -z "$2" ]; then
			echo 'error: [-a] missing AT device name'
			exit 4
		fi
		atdev=$2
		shift
		shift
		;;
	*)
		break;
		;;
	esac
done

# entry

case $1 in
status)
	s_on=$(cat $slotb/power)
	s_rst=$(($(cat $slotb/reset) ^ 1))
	s_sim=$(($(cat $slotb/sim_select) + 1))
	if [ "$s_sim" -eq 1 ]; then
		s_tb=bottom
	else
		s_tb=top
	fi
	echo "cell: power=$s_on reset=$s_rst sim$s_sim ($s_tb)"
	;;
kill)
	cell_kill;
	;;
off)
	cell_off;
	;;
on)
	cell_on 1;
	;;
reset)
	echo 0 > $slotb/reset
	sleep 10
	echo 1 > $slotb/reset
	wait_on 30
	;;
ids)
	qmicli -d $qmidev --dms-get-ids
	;;
imei)
	qmicli -d $qmidev --dms-get-ids | grep IMEI | awk -e '{print $2}' 
	;;
usb-qmi)
	set_usb 6
	;;
usb-mbim)
	set_usb 8
	;;
fw-info)
	qmicli -d $qmidev --dms-list-stored-images
	qmicli -d $qmidev --dms-swi-get-current-firmware
	qmicli -d $qmidev --dms-get-firmware-preference
	;;
sim)
	if [ "$2" = "enable" ]; then
		echo 1 > $slotb/sim_enable
		echo "sim enabled"
	elif [ "$2" = "disable" ]; then
		echo 0 > $slotb/sim_enable
		echo "sim disabled"
	elif [ "$2" = "plug" ]; then
		echo 0 > $slotb/sim_enable
		sleep 0.5
		echo 1 > $slotb/sim_enable
	elif [ "$2" = "info" ]; then
		qmicli -d $qmidev --uim-get-card-status
	elif [ "$2" = "detect" ]; then
		qmicli -d $qmidev --uim-get-card-status | awk -e '/Card state:/ { if(last == "Card [0]:") { print }} { last = $0 }'
	elif [ "$2" = "both" ]; then
		echo 0 > $slotb/sim_enable
		sleep 0.5
		echo 0 > $slotb/sim_select
		sleep 1
		echo 1 > $slotb/sim_enable
		sleep 1
		qmicli -d $qmidev --uim-get-card-status | awk -e '/Card state:/ { if(last == "Card [0]:") { print "sim1:" $0 }} { last = $0 }'
		echo 0 > $slotb/sim_enable
		sleep 0.5
		echo 1 > $slotb/sim_select
		sleep 1
		echo 1 > $slotb/sim_enable
		sleep 1
		qmicli -d $qmidev --uim-get-card-status | awk -e '/Card state:/ { if(last == "Card [0]:") { print "sim2:" $0 }} { last = $0 }'
	else
		echo "error: usage: $0 sim [enable|disable|plug|info|detect|both]"
		exit 5
	fi
	;;
sim1)
	echo 0 > $slotb/sim_enable
	echo 0 > $slotb/sim_select
	echo 1 > $slotb/sim_enable
	echo "sim1 (bottom) selected"
	;;
sim2)
	echo 0 > $slotb/sim_enable
	echo 1 > $slotb/sim_select
	echo 1 > $slotb/sim_enable
	echo "sim2 (top) selected"
	;;
test)
	if [ "$2" = "enable" ]; then
		cell_kill
		sleep 5
		rm /etc/hotplug.d/tty/30-velocloud-modem-tty 
		echo "test mode enabled"
	elif [ "$2" = "disable" ]; then
		cp /root/30-velocloud-modem-tty /etc/hotplug.d/tty/30-velocloud-modem-tty 
		/etc/init.d/ModemManager start > /dev/null 2>&1
		cell_off
		cell_on 1
		echo "test mode disabled"
	else
		echo "error: usage: $0 test [enable|disable]"
		exit 5
	fi
	;;
at)
	echo "picocom for AT command mode, use <CTRL-a CTRL-a CTRL-q> to quit"
	picocom $atdev
	;;
help)
	echo "usage: $0 [options] command"
	echo " -q qmi-dev    - QMI device name, default /dev/cdc-wdm0"
	echo " -a at-dev     - AT device name, default /dev/ttyUSB2"
	echo "  status       - list power/reset status"
	echo "  kill         - kill all network stack programs using the modem"
	echo "  off          - power off modem, wait for it to be gone"
	echo "  on           - power on modem, wait for it to appear"
	echo "  reset        - hard-reset the modem without power cycle"
	echo "  ids          - dump all modem module IDs"
	echo "  imei         - dump just the IMEI"
	echo "  usb-qmi      - set usb composition to QMI mode"
	echo "  usb-mbim     - set usb composition to MBIM mode"
	echo "  fw-info      - dump firmware images and info"
	echo "  sim disable  - enable the current SIM slot, disable before switching SIMs"
	echo "  sim enable   - enable the current SIM slot, enable after switching SIMs"
	echo "  sim plug     - tell modem that a sim plug/unplug event has happened"
	echo "  sim info     - print SIM info of currently selected slot"
	echo "  sim detect   - print if SIM is detected in currently selected slot"
	echo "  sim both     - print detection status of both SIM cards"
	echo "  sim1         - select SIM1 (bottom) via on-board mux"
	echo "  sim2         - select SIM2 (top) via on-board mux"
	echo "  test disable - disable test mode, brings up the modem network stack"
	echo "  test enable  - enable test mode, tears down the modem network stack"
	echo "  at           - opens a picocom to issue AT commands"
	echo "                 be careful when modem network stack is up"
	;;
*)
	echo "error: invalid command: $1"
	exit 3
	;;
esac

exit 0

