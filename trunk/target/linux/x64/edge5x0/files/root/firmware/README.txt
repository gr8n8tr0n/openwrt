The files here are different for the Edge 520/540 ("5x0") and the Edge 510.

Edge 510:
Coreboot image:	510/2017-4-10-coreboot.rom
PIC image:	510/2017-4-12-pic.hex

Edge 520/540:
Coreboot image:	5x0/vc5x0-cb-01-bootorder.rom
