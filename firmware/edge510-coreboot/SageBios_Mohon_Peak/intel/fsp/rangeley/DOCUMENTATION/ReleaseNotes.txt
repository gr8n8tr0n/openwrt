================================================================================
Intel(R) Firmware Support Package (Intel� FSP) for Rangeley
Release Notes

Release label: RANGELEY_FSP_POSTGOLD_001_20131218
December 18, 2013
================================================================================
================================================================================
Copyright (c)  2013, Intel Corporation.
This Intel� Firmware Support Package ("Software") is furnished under license and
may only be used or copied in accordance with the terms of that license.
No license, express or implied, by estoppel or otherwise, to any intellectual
property rights is granted by this document. The Software is subject to change
without notice, and should not be construed as a commitment by Intel Corporation
to market, license, sell or support any product or technology. Unless otherwise
provided for in the license under which this Software is provided, the Software
is provided AS IS, with no warranties of any kind, express or implied.
Except as expressly permitted by the Software license, neither Intel Corporation
nor its suppliers assumes any responsibility or liability for any errors or
inaccuracies that may appear herein. Except as expressly permitted by the
Software license, no part of the Software may be reproduced, stored in a
retrieval system, transmitted in any form, or distributed by any means without
the express written consent of Intel Corporation.
================================================================================
================================================================================
RELEASE NOTES CONTENTS
================================================================================
1.   OVERVIEW
2.   RELEASE INFORMATION
3.   CONFIGURATION
4.   RMT SUPPORT
5.   LIMITATIONS
6.   CHANGE LOG
================================================================================
1.  OVERVIEW
================================================================================
This package contains required binary image(s) and collateral for the
Intel(R) Firmware Support Package (Intel� FSP) for Rangeley.
================================================================================
2.  RELEASE INFORMATION
================================================================================
This release supports the Simics Avoton Virtual Platform and Rangeley Silicon.
It contains one binary image:
   FvFsp.bin   - The FSP binary to use on Rangeley based platforms and on the
                 Simics Avoton Virtual Platform.
================================================================================
3.  CONFIGURATION
================================================================================
A Binary Configuration Tool for Intel� FSP is provided as a companion tool and
is intended to be used to:

  - Change configuration options based on provided BSF and FSP.
  - Rebase the FSP to a different Base Address.
Please refer to the Binary Configuration Tool User Guide for the usage
instructions.
The BCT will be available on CDI. Please contact your Intel Representative.
================================================================================
4.  RMT SUPPORT
================================================================================
To enable Rank Margining Tool (RMT) according to document
535399 RMT User Guid v0.9 (Please contact Intel Representative):
  - Open BSF file with BCT
  - In North Complex:
    - "Enable Rank Margin Tool" -> Enabled
    - "RMT CPGC exp_loop_cnt" -> 13
    - "RMT CPGC num_bursts" -> 13
  - In Platform Settings:
    - "Enable Serial Debug Messages" -> Enabled
  - Save and patch the FvFsp.bin

================================================================================
5.  LIMITATIONS
================================================================================
- For Rangeley Platform the Serial Port is at port 0x2F8.

================================================================================
6.  CHANGE LOG
================================================================================
Release EDISONVILLE_DEV_FSP_001_20130124:  (January 24, 2013)
 - Supports Simics Avoton Virtual Platform only.
 - Initial Evaluation Release.
Release RANGELEY_DEV_FSP_002_20130320: (March 20, 2013)
 - Supports Rangeley based platforms and Simics Avoton Virtual Platform.
 - Updated to reference code (v0.67) from WW10 AVN BIOS OEM Release.
Release RANGELEY_DEV_FSP_003_20130426: (April 26, 2013)
 - Updated to reference code (v0.70) from WW14 AVN BIOS OEM Release.
 - Added NvsBufferPtr capability to enable FSP structures to be saved/restored
   from NVRAM. Added BootMode parameter to FspInitEntry API.
 - Added MPInit Driver support.
 - Updated PCDs.
Release RANGELEY_DEV_FSP_004_20130517: (May 17, 2013)
 - Updated Image Revision in FSP Header (Major 0 Minor 0x76).
 - Added Self-Check to only allow Rangeley FSP run on Rangeley SoC.
 - Update to reference code (v0.71) from WW16 AVN BIOS OEM Release.
 - Added BSF file to release package.
 - Added PCD to Enable/Disable DEBUG messages in FSP, configurable via Binary
   Configuration Tool.
Release RANGELEY_DEV_FSP_005_20130717: (July 17, 2013)
 - Updated Image Revision in FSP Header (Major 0 Minor 0x80).
 - Supports one binary image only for both Rangeley Silicon and Simics Virtual
   Platform.
 - Update to reference code (v0.80) from WW28 AVN BIOS OEM Release.
 - Added PCD to Enable/Disable FastBoot in FSP, configurable via BCT
Release RANGELEY_GOLD_FSP_006_RC1_20130909: (September 09, 2013)
 - Gold Release Candidate 1.
 - Updated Image Revision in FSP Header (Major 1 Minor 0x0).
 - Update to reference code from WW35 AVN BIOS OEM Release.
 - Added Memory Down support.
 - Removed GPIO reconfiguration that occurred during FspInit.
Release RANGELEY_GOLD_FSP_006_RC1_20131004: (October 04, 2013)
 - Removed SMBus PCD.
 - Added FSP description file, used by BCT.
Release RANGELEY_DEV_FSP_007_20131009: (October 09, 2013)
 - Added RMT support.
Release RANGELEY_DEV_FSP_009_20131127: (November 27, 2013)
 - Added SPD Write Protect PCD.
 - Added ECC PCD.
 - Updated to reference code from WW47 AVN BIOS OEM Release.
Release RANGELEY_DEV_FSP_010_20131204: (December 04, 2013)
 - Updated to reference code from WW48 AVN BIOS OEM Release.
Release RANGELEY_FSP_RELEASE_001_RC1_20131209: (December 09, 2013)
 - Updated to reference code from WW49 AVN BIOS OEM Release.
 - Removed unused PCD from VPD Region.
Release RANGELEY_FSP_RELEASE_003_RC2_20131213: (December 13, 2013)
 - Fixed TSeg PCD settings.
 - Enabled additional DEBUG messages.
Release RANGELEY_FSP_POSTGOLD_001_20131218: (December 18, 2013)
 - Rangeley FSP Post Gold Release.
================================================================================
INTEL CONFIDENTIAL
================================================================================
