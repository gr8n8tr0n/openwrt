##
## This file is part of the coreboot project.
##
## Copyright (C) 2013-2014 Sage Electronic Engineering, LLC.
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; version 2 of the License.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program; if not, write to the Free Software
## Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
##


config CPU_INTEL_FSP_MODEL_206AX
	bool

config CPU_INTEL_FSP_MODEL_306AX
	bool

if CPU_INTEL_FSP_MODEL_206AX || CPU_INTEL_FSP_MODEL_306AX

config CPU_SPECIFIC_OPTIONS
	def_bool y
	select PLATFORM_USES_FSP
	select ARCH_BOOTBLOCK_X86_32
	select ARCH_ROMSTAGE_X86_32
	select ARCH_RAMSTAGE_X86_32
	select SMP
	select SSE2
	select UDELAY_LAPIC
	select SMM_TSEG
	select SUPPORT_CPU_UCODE_IN_CBFS if HAVE_FSP_BIN
	select PARALLEL_CPU_INIT
	select TSC_SYNC_MFENCE
	select SAVE_EARLY_TIMESTAMPS_TO_CMOS if COLLECT_TIMESTAMPS

config BOOTBLOCK_CPU_INIT
	string
	default "cpu/intel/fsp_model_206ax/bootblock.c"

config SMM_TSEG_SIZE
	hex
	default 0x800000

config ENABLE_VMX
	bool "Enable VMX for virtualization"
	default n

config CPU_MICROCODE_CBFS_LOC
	hex
	depends on SUPPORT_CPU_UCODE_IN_CBFS
	default 0xfff70040

config MICROCODE_INCLUDE_PATH
	string "Location of the intel microcode patches"
	default "../intel/cpu/ivybridge/microcode" if CPU_INTEL_FSP_MODEL_306AX
	default "../intel/cpu/sandybridge/microcode" if CPU_INTEL_FSP_MODEL_206AX

config USE_COREBOOT_MTRR_SETUP
	bool "Use coreboot MTRR setup instead of the FSP's"
	default y
	help
		The FSP MTRR setup for this platform has issues with
		memory above 4 GB. The coreboot MTRR setup does not
		exhibit the same problem and significantly increases
		the performance of the platform.

config EXCLUDE_306A2_MICROCODE
	bool
	depends on SUPPORT_CPU_UCODE_IN_CBFS
	default n

config EXCLUDE_306A4_MICROCODE
	bool
	depends on SUPPORT_CPU_UCODE_IN_CBFS
	default n

config EXCLUDE_306A5_MICROCODE
	bool
	depends on SUPPORT_CPU_UCODE_IN_CBFS
	default n

config EXCLUDE_306A8_MICROCODE
	bool
	depends on SUPPORT_CPU_UCODE_IN_CBFS
	default n

config EXCLUDE_306A9_MICROCODE
	bool
	depends on SUPPORT_CPU_UCODE_IN_CBFS
	default n

config DISABLE_TURBO
	bool
	default n

endif
