#!/bin/bash
#
# This file is part of the coreboot project.
#
# Copyright (C) 2007-2010 coresystems GmbH
# Copyright (C) 2012 Google Inc
# Copyright (C) 2013-2014 Sage Electronic Engineering, LLC.
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; version 2 of the License.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
#

TMPFILE=""
if [ "$SAGE_HOME" == "" ]; then
	echo '$(error ERROR: SageEDK build tools were not found.  Please visit http://www.se-eng.com/downloads.html or contact sales@se-eng.com to obtain a copy of the SageEDK.)'
	exit 1
fi

# Search for the SageBIOS toolchain where it is installed for the SageEDK.
if [[ $OSTYPE == "cygwin" && "$SAGE_HOME" != "" ]]; then
	# Convert $SAGE_HOME from Windows style path, if necessary
	SAGE_EDK_DIR=$(cygpath --unix $SAGE_HOME)
else
	SAGE_EDK_DIR=$SAGE_HOME
fi
SAGE_EDK_TOOLS="$SAGE_EDK_DIR/tools/xgcc/bin/"

die() {
	echo "ERROR: $*" >&2
	exit 1
}

clean_up() {
	if [ -n "$TMPFILE" ]; then
		rm -f "$TMPFILE" "$TMPFILE.c" "$TMPFILE.o"
	fi
}

# Create temporary file(s).
TMPFILE="$(mktemp /tmp/temp.XXXX 2>/dev/null || echo /tmp/temp.78gOIUGz)"
touch "$TMPFILE"
trap clean_up EXIT


program_exists() {
	type "$1" >/dev/null 2>&1
}

for gccprefixes in ${SAGE_EDK_TOOLS}; do
	if [ "`${gccprefixes}iasl 2>/dev/null | grep -c ACPI`" -gt 0 ]; then
		IASL=${gccprefixes}iasl
		break
	else
		echo '$(error ERROR: Toolchain is invalid.  Please reinstall a licensed copy of the SageEDK or contact support@se-eng.com.)'
		exit 1
	fi
done

if program_exists gcc; then
	HOSTCC=gcc
elif program_exists cc; then
	HOSTCC=cc
else
	echo "no host compiler found"
	exit 1
fi

cat <<EOF
# platform agnostic and host tools
IASL:=${IASL}
HOSTCC:=${HOSTCC}

EOF

testcc() {
	local tmp_c="$TMPFILE.c"
	local tmp_o="$TMPFILE.o"
	rm -f "$tmp_c" "$tmp_o"
	echo "void _start(void) {}" >"$tmp_c"
	"$1" -nostdlib -Werror $2 "$tmp_c" -o "$tmp_o" >/dev/null 2>&1
}

testas() {
	local gccprefixes="$1"
	local twidth="$2"
	local arch="$3"
	local use_dash_twidth="$4"
	local obj_file="$TMPFILE.o"
	local full_arch="elf$twidth-$arch"

	rm -f "$obj_file"
	[ -n "$use_dash_twidth" ] && use_dash_twidth="--$twidth"
	${gccprefixes}as $use_dash_twidth -o "$obj_file" $TMPFILE 2>/dev/null ||
		return 1

	# Check output content type.
	local obj_type="$(${gccprefixes}objdump -p $obj_file)"
	local obj_arch="$(expr "$obj_type" : '.*format \(.[a-z0-9-]*\)')"
	[ "$obj_arch" = "$full_arch" ] || return 1

	# Architecture matched.
	GCCPREFIX="$gccprefixes"

	if [ -z "$use_dash_twidth" ]; then
		ASFLAGS=""
		CFLAGS=""
		LDFLAGS=""
	else
		ASFLAGS="--$twidth"
		CFLAGS="-m$twidth"
		LDFLAGS="-b $full_arch"

	fi

	# Special parameters only available in dash_twidth mode.
	[ -n "$use_dash_twidth" ] && case "$full_arch" in
		"elf32-i386" )
			LDFLAGS="$LDFLAGS -melf_i386"
			CFLAGS="$CFLAGS -Wl,-b,elf32-i386 -Wl,-melf_i386"
			;;
	esac

	return 0
}

detect_special_flags() {
	local architecture="$1"
	# GCC 4.6 is much more picky about unused variables.
	# Turn off it's warnings for now:
	testcc "$CC"   "$CFLAGS -Wno-unused-but-set-variable " &&
		CFLAGS="$CFLAGS -Wno-unused-but-set-variable "

	# Use bfd linker instead of gold if available:
	testcc "$CC"   "$CFLAGS -fuse-ld=bfd" &&
		CFLAGS="$CFLAGS -fuse-ld=bfd" && LINKER_SUFFIX='.bfd'

	testcc "$CC"   "$CFLAGS -fno-stack-protector"&&
		CFLAGS="$CFLAGS -fno-stack-protector"
	testcc "$CC"   "$CFLAGS -Wl,--build-id=none" &&
		CFLAGS="$CFLAGS -Wl,--build-id=none"

	case "$architecture" in
	x86)
		testcc "$CC"   "$CFLAGS -Wa,--divide" &&
			CFLAGS="$CFLAGS -Wa,--divide"
			;;
	esac
}

report_arch_toolchain() {
	cat <<EOF
# elf${TWIDTH}-${TBFDARCH} toolchain (${GCCPREFIX}gcc)
CC_${TARCH}:=${GCCPREFIX}gcc
CFLAGS_${TARCH}:=${CFLAGS}
CPP_${TARCH}:=${GCCPREFIX}cpp
AS_${TARCH}:=${GCCPREFIX}as ${ASFLAGS}
LD_${TARCH}:=${GCCPREFIX}ld${LINKER_SUFFIX} ${LDFLAGS}
NM_${TARCH}:=${GCCPREFIX}nm
OBJCOPY_${TARCH}:=${GCCPREFIX}objcopy
OBJDUMP_${TARCH}:=${GCCPREFIX}objdump
READELF_${TARCH}:=${GCCPREFIX}readelf
STRIP_${TARCH}:=${GCCPREFIX}strip
AR_${TARCH}:=${GCCPREFIX}ar

EOF
}

# Architecture definition
SUPPORTED_ARCHITECTURE="x86 armv7 aarch64"

arch_config_armv7() {
	TARCH="armv7"
	TBFDARCH="littlearm"
	TCLIST="armv7a armv7-a"
	TWIDTH="32"
	TABI="eabi"
}

arch_config_aarch64() {
	TARCH="aarch64"
	TBFDARCH="littleaarch64"
	TCLIST="aarch64"
	TWIDTH="64"
	TABI="eabi"
}

arch_config_x86() {
	TARCH="x86_32"
	TBFDARCH="i386"
	TCLIST="i386 x86_64"
	TWIDTH="32"
	TABI="elf"
}

test_architecture() {
	architecture=$1

	GCCPREFIX="invalid"
	if type arch_config_$architecture > /dev/null; then
		arch_config_$architecture
	else
		echo "no architecture definition for $architecture"
		exit 1
	fi

	# To override toolchain, define CROSS_COMPILE_$arch or CROSS_COMPILE as
	# environment variable.
	# Ex: CROSS_COMPILE_arm="armv7a-cros-linux-gnueabi-"
	#     CROSS_COMPILE_x86="i686-pc-linux-gnu-"
	search="$(eval echo \$CROSS_COMPILE_$architecture 2>/dev/null)"
	search="$search $CROSS_COMPILE"
	for toolchain in $TCLIST; do
		search="$search $SAGE_EDK_TOOLS$toolchain-$TABI-"
		search="$search $toolchain-$TABI-"
		search="$search $toolchain-linux-gnu-"
	done
	echo "# $architecture TARCH_SEARCH=$search"

	# Search toolchain by checking assembler capability.
	for gccprefixes in $search ""; do
		program_exists "${gccprefixes}as" || continue
		testas "$gccprefixes" "$TWIDTH" "$TBFDARCH" "" && break
		testas "$gccprefixes" "$TWIDTH" "$TBFDARCH" "TRUE" && break
	done

	if [ "$GCCPREFIX" = "invalid" ]; then
		if [ "$architecture" == "x86" ]; then
			echo '$(error ERROR: Toolchain is invalid.  Please reinstall a licensed copy of the SageEDK or contact support@se-eng.com.)'
			exit 1
		else
			# Do not echo a warning about missing ARM compiler because SageBIOS
			# does not currently support any ARM based platforms.
			# echo "Warning: no suitable GCC for $architecture." >&2
			continue
		fi
	fi
	CC="${GCCPREFIX}"gcc

	detect_special_flags "$architecture"
	report_arch_toolchain
}

# This loops over all supported architectures.
for architecture in $SUPPORTED_ARCHITECTURE; do
	test_architecture $architecture
done

echo ''
echo '#Create a false target pointing to the old initial target'
echo 'first_target: real-target'
echo ''

echo 'final-toolcheck:'
echo '	echo ""'

# check for recommended system tools and warn users if they are not correct
sh_bash=`/bin/sh --version 2>/dev/null | grep -i 'bash'`
awk_gawk=`awk --version 2>/dev/null | grep -i 'GNU awk'`
if [[ $sh_bash == "" || $awk_gawk == "" ]]; then
	echo '	echo "*************************** WARNING ***************************"'
	echo '	echo "*                                                             *"'

	if [[ $sh_bash == "" ]]; then
		echo '	echo "* Sage recommends that /bin/sh to be bash, but it is not.     *"'
		echo '	echo "* This can cause unpredictable results in the make and in     *"'
		echo '	echo "* shell scripts associated with the build.                    *"'
		echo '	echo "* For Ubuntu, reconfigure from dash to bash by running        *"'
		echo '	echo "* the following command and selecting *NO* when prompted.     *"'
		echo '	echo "* sudo dpkg-reconfigure dash                                  *"'
	fi

	if [[ $sh_bash == "" && $awk_gawk == "" ]]; then
		echo '	echo "*                                                             *"'
	fi

	if [[ $awk_gawk == "" ]]; then
		echo '	echo "* Sage recommends that /bin/awk point to GNU Awk, but it      *"'
		echo '	echo "* does not.                                                   *"'
		echo '	echo "* Please install gawk to get rid of this warning.             *"'
		echo '	echo "* Debian/Ubuntu/Mint: sudo apt-get install gawk               *"'
		echo '	echo "* Fedora/Centos/RHEL: sudo yum install gawk                   *"'
	fi

	echo '	echo "*                                                             *"'
	echo '	echo "***************************************************************"'
	echo ''
fi

